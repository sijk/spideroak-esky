Esky VersionFinder for SpiderOak share rooms
============================================

Esky_ is an auto-update framework for frozen python applications. SpiderOak_ is
an online backup/sync provider. This module allows you to use SpiderOak to
distribute your Esky updates. 

.. _Esky: http://pypi.python.org/pypi/esky
.. _SpiderOak: https://spideroak.com/

Usage Example
~~~~~~~~~~~~~

Suppose your SpiderOak share ID is "johnsmith" and that you're developing
your project in ``~/my_project``. Open up SpiderOak, make sure that
``~/my_project/dist`` is ticked in the *Backup* tab and click *Save*. Then
create a share room with a room key of, for instance, "myproject" and add
``~/my_project/dist`` to the room. In your program you would then add
something like::

    if hasattr(sys, 'frozen'):
        finder = SpiderOakVersionFinder('johnsmith', 'myproject')
        app = esky.Esky(sys.executable, finder)

Whenever you run ``python setup.py bdist_esky`` SpiderOak will automatically
upload the resulting bundle/patch, making it instantly available to clients.

