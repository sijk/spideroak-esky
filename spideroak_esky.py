from esky.finder import DefaultVersionFinder
from esky.bootstrap import join_app_version
from base64 import b32encode
import json
import re

# Read https://spideroak.com/faq/questions/37/how_do_i_use_the_spideroak_web_api/
# if you're going to modify this class.

class SpiderOakVersionFinder(DefaultVersionFinder):
    '''
    This `VersionFinder` looks for esky bundles in a SpiderOak share room.
    It assumes that the share room is set to share a single folder containing
    the esky bundles/patches. The name of the folder is not important. 
    '''
    def __init__(self, share_id, room_key):
        self.download_url = '/'.join(('https://spideroak.com/share',
                                      b32encode(share_id).rstrip('='),
                                      room_key, ''))

        super(SpiderOakVersionFinder, self).__init__(self.download_url)

    def _list_files(self):
        response = self.open_url(self.download_url)
        try:
            listing = response.read().decode("utf-8")
            listing = json.loads(listing)
        finally:
            response.close()

        if 'files' not in listing:
            # Look for files in the first subdirectory of the share room
            self.download_url += listing['dirs'][0][1]
            return self._list_files()

        return listing['files']

    def find_versions(self, app):
        version_re = "[a-zA-Z0-9\\.-_]+"
        appname_re = "(?P<version>%s)" % (version_re,)
        appname_re = join_app_version(app.name,appname_re,app.platform)
        filename_re = "%s\\.(zip|exe|from-(?P<from_version>%s)\\.patch)"
        filename_re = filename_re % (appname_re,version_re,)

        files = self._list_files()

        for f in files:
            match = re.match(filename_re, f['name'])
            if match:
                version = match.group('version')
                from_version = match.group('from_version')
                cost = 40 if from_version is None else 1
                self.version_graph.add_link(from_version or "",version,f['url'],cost)

        return self.version_graph.get_versions(app.version)

